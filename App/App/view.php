<?php

/* CONFIGURE TWIG VIEWS */
call_user_func(function() use($app){
	$templateFolder = __DIR__ . '/templates';
	$app->config(array(
		'view' => new \Slim\Views\Twig()
	));
	$view = $app->view();
	$view->twigTemplateDirs = array("{$templateFolder}/views");
	$view->parserOptions = array(
		'debug' => true,
		'cache' => "{$templateFolder}/cache"
	);
	$view->parserExtensions = array(
		new \Slim\Views\TwigExtension(),
	);
});
/*******************/