<?php

namespace App\Controller;


class Base{

	protected $app;
	
	public function __construct(&$app){
		$this->app = $app;
	}
	
	public function callAction($action, $params){
		$actionName = strtolower($action)."Action";
		if(method_exists($this , $actionName)){
			return call_user_func_array(array($this, $actionName), $params);
		}else{
			$this->app->notFound();
		}
	}
	
	protected function getEntityManager(){
		return $this->app->doctrine->getEntityManager();
	}

}