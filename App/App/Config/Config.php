<?php

namespace App\Config;

class Config{
	
	/**
	 * @var \Slim\Slim
	 */
	protected $app;
	protected $environment;
	protected $options;
	
	public function __construct($environment){
		$this->environment = $environment;
	}
	
	public function setApp(\Slim\Slim $app){
		$this->app = $app;
		$this->configureModes();
		return $this->app;
	}
	
	/*public function getMode(){
		return APPLICATION_ENV;
	}*/
	
	public function getOptions($section = NULL){
		$filename = "ini/config";
		if(!$this->options){
			$options = parse_ini_file("{$filename}.ini", true);
			$options2 = @parse_ini_file("{$filename}.{$this->environment}.ini", true);
			if(is_array($options2)){
				$options = array_replace_recursive($options, $options2);
			}
			$this->options = &$options;
		}
		if($section){
			return $this->options[$section];
		}
		return $this->options;
	}
	
	public function getRootUri(){
		return $this->app->request()->getRootUri();
	}
	
	public function configureModes(){
		
		$options = $this->getOptions('slim');
		
		$this->app->config(array(
			'config' => $this,
			'debug' => $options['debug'] == '1',
			'log.enable' => $options['log.enable'] == '1'
		));
		
		return $this;
		
	}
	
	
}