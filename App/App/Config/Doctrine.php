<?php

namespace App\Config;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Doctrine
{

	private $em;

	public function __construct($config){
		$dbParams = array(
			'driver'        => $config['driver'],
			'user'          => $config['user'],
			'password'      => $config['password'],
			'host'          => $config['host'],
			'dbname'        => $config['dbname'],
			'charset'       => $config['charset'],
			'driverOptions' => array(
				'charset'   => $config['charset'],
			)
		);
		
		$dev = $config['devmode'] == '1';
		$entitiesPath  = array(getcwd() . "/App/Model/Entity");
		$proxiesPath  = "{$entitiesPath}/Proxy";
		
		$config = Setup::createAnnotationMetadataConfiguration($entitiesPath, $dev, $proxiesPath);
		$entityManager = EntityManager::create($dbParams, $config);
		$this->em = $entityManager;
		
	}
	
	public function getEntityManager(){
		return $this->em;
	}

}