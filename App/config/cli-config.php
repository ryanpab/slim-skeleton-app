<?php

//chdir('../../');

$loader = require 'vendor/autoload.php';
$loader->add('App', getcwd());

$config = new App\Config\Config('testing');

$doctrine = new App\Config\Doctrine($config->getOptions('doctrine'));

use Doctrine\ORM\Tools\Console\ConsoleRunner;
return ConsoleRunner::createHelperSet($doctrine->getEntityManager());