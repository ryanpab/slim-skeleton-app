<?php

namespace App\Controller;


class Index extends Base{

	public function indexAction($page = 1){
		
		$entity =  "App\\Model\\Entity\\Survey";
		
		$repo = $this->getEntityManager()->getRepository($entity);
		$data = $repo->findBy(array('name' => 'survey 1'));
		
		$name = $data[0]->getName();
		return $this->app->render(
			'test/test.html',
			array(
				'name' => $name		
			)
		);
	}

}