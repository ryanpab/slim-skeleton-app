<?php
namespace App\Test;

use Slim\Environment;
use App\Config\Config;
use App\Config\Doctrine;

abstract class ControllerBase extends \PHPUnit_Framework_TestCase  
{

	protected function getApp(){
		$config = new Config("testing");
		$app = $config->setApp(new \Slim\Slim());
		$app->container->singleton('doctrine', function () use($config) {
			return new Doctrine($config->getOptions('doctrine'));
		});
		require '../App/App/view.php';
		require '../App/App/routes.php';
		return $app;
	}
	
	public function request($method, $path, $options = array()){
        // Capture STDOUT
        ob_start();

        // Prepare a mock environment
        Environment::mock(array_merge(array(
            'REQUEST_METHOD' => $method,
            'PATH_INFO' => $path,
            'SERVER_NAME' => 'slim-test.dev',
        ), $options));

        $app = $this->getApp();
        $app->run();
        
        $this->app = $app;
        $this->request = $app->request();
        $this->response = $app->response();

        // Return STDOUT
        return ob_get_clean();
    }

    public function get($path, $options = array()){
        $this->request('GET', $path, $options);
    }

}