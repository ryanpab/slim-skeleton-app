<?php


chdir(__DIR__);

$loader = require 'vendor/autoload.php';
$loader->add('App', __DIR__);


use App\Config\Config;
use App\Config\Doctrine;

$config = NULL;

call_user_func(function() use(&$config){

	$environment = "development";
	$config = new Config($environment);

});



/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */

$app = $config->setApp(new \Slim\Slim());
$app->setName('main');

$app->container->singleton('doctrine', function () use($config) {
	return new Doctrine($config->getOptions('doctrine'));
});

require 'App/view.php';
require 'App/routes.php';


/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
