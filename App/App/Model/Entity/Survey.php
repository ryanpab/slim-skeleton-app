<?php
namespace App\Model\Entity;

/**
 * @Entity(repositoryClass="App\Model\Repository\Survey")
 * @Table(name="`survey`")
 */
class Survey
{
    /**
     * @Id @Column(type="integer", name="`id`")
     * @GeneratedValue
     */
    private $id;
    
  	/** @Column(type="string", name="`name`") */
    private $name;
    
    
	public function getId(){return $this->id;}

    public function getName(){return $this->name;}
    public function setName($value){$this->name = $value;return $this;}

}